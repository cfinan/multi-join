# Getting Started with multi-join

__version__: `0.1.1a0`

The multi-join package is a toolkit to join any number of iterables together using specified join conditions. It is mainly geared towards joining delimited flat files together though, similar to unix `join` command but in a pure python API. It is built on top of `heapq.merge`.`

There is [online](https://cfinan.gitlab.io/multi-join/index.html) documentation for multi-join.

## Installation instructions
At present, multi-join is undergoing development and no packages exist yet on PyPi. Therefore it is recommended that it is installed in either of the two ways listed below.

### Installation using pip
You can install using pip from the root of the cloned repository, first clone and cd into the repository root:

```
git clone git@gitlab.com:cfinan/multi-join.git
cd multi-join
```

Install the dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

Then install using pip
```
python -m pip install .
```

Or for an editable (developer) install run the command below from the root of the multi-join repository. The difference with this is that you can just to a `git pull` to update multi_join, or switch branches without re-installing:
```
python -m pip install -e .
```

### Installation using conda
I maintain a conda package in my personal conda channel. To install this please run:

```
conda install -c cfin -c conda-forge multi-join
```

### Conda dependencies
There are also conda yaml environment files in `./resources/conda/env` that have the same contents as `requirements.txt` but for conda packages, so all the pre-requisites. I use this to install all the requirements via conda and then install the package as an editable pip install.

However, if you find these useful then please use them. There are Conda environments for Python v3.8, v3.9 and v3.10.

## Basic usage
There are some examples in `./resources/examples` where `.` is the root of the multi-join repository.

## Run tests
If you have cloned the repository, you can also run the tests using `pytest ./tests`, if any fail please contact us (see the contribution page for contact info).
