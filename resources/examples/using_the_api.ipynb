{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0c9a1a79",
   "metadata": {},
   "source": [
    "# Using the ``multi_join`` API\n",
    "The multi_join API is designed to make it relatively easy to join two or more Python Iterators based on specific key definitions (in a similar way to `sorted`). Whilst it is defined with files in mind, it can be used on any iterators and iterables that can be made into iterators, for example, strings and lists. The only requirement is that your iterable must be sorted on your key columns (i.e. similar to the requirement for the unix `join` command). It also allows you to precisely define how individual iterators should be joined and at what point you want to stop the joining process.\n",
    "\n",
    "This document uses a basic example of joining strings to illustrate the usage, however, if the iterators are files then the process is similar with the exception that the files need to be opened and closed by the user. Examples of this will be added in future. [API documentation](https://cfinan.gitlab.io/multi-join/join.html) is also available.\n",
    "\n",
    "## The API components of of a join\n",
    "There are three main components of the join:\n",
    "\n",
    "1. The is a join iterator class, `multi_join.join.JoinIterator` that wraps the iterator and provides the objects and keys into the join.\n",
    "2. The are monitor classes that accept `JoinIterator` arguments and will tell the join process when to exit. This allows the user to specify the join process to exit early when one or a subset of iterators is exhausted it's contents. These are `multi_join.join.AllIterMonitor` and `multi_join.join.AnyIterMonitor`. The `AllIterMonitor` will exit the join condition when all of the `JoinIterators` within it have exhaused their contents. The `AnyIterMonitor` will exit the join process when one of the `JoinIterators` within it has exhaused it's contents. There is also the option of no monitoring that will allow the join process to continue until the final iterator has exhausted it's contents.\n",
    "3. The main `multi_join.join.Join` class that accepts `JoinIterator` objects and will provide joined rows.\n",
    "\n",
    "In addition to these components there are some constants that are supplied to the `JoinIterator` that define what constitutes a join.\n",
    "\n",
    "1. `multi_join.join.NO_CONDITION` specifies that a join only occurs if the key occurs in all of the iterators.\n",
    "2. `multi_join.join.INTERSECT` specifies that a join occurs when all of the iterators carrying the `INTERSECT` flag are joined. If any other iterators also involved in the join, they will be returned as well but they are passive and do not have any input as to what constitutes the join.\n",
    "3. `multi_join.join.RETURN_ALL` specifies that iterators carrying this flag should always return their objects (rows) irrespective of any joins with other iterators. This is similar to either a left/right join in a database. As with intersect, if any of the other iteractors share the join they will return their objects as well but they do not determine what constitutes the join.\n",
    "\n",
    "The monitors and constants that are supplied to them determine when the join exists and what/how many of the iterators that match up to constitute a join. However, the join `key` tells the join process exactly what attributes of the objects within the iterator should be used for the join. This works in a very similar way to the Python `sorted` command but with a few changes.\n",
    "\n",
    "If the key is `None`, then the whole object is placed into the join. However, the key can also be integers that are interpreted as index positions in a list, or strings that are interpreted as keys in a dictionary. In addition, the key can be a function/lambda that is used to derive the keys to join on. \n",
    "\n",
    "With the main API components defined, now we will look at some very simple examples that can be followed to see the impact of the join flags and monitors."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3122b4a8",
   "metadata": {},
   "source": [
    "## Joining strings\n",
    "Perhaps the simplest way to learn the options is to join strings together and look at the output. In Python strings are not naturally iterators but can be made into iterators with a call to `iter()`. However, this happens internally within the `joinIterator` so the user does not need to be concerned with this.\n",
    "\n",
    "When used as an iterator, the unit of iteration is each character within the string. So the join will be based on characters. As each character is the object we want to join on, in these examples we do not need to specify a join key. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8e8a82f9",
   "metadata": {},
   "source": [
    "### A basic join\n",
    "We will start with the simplest scenario with two strings where everything matches but some characters occur more than once in one string than the other."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "3423cf7e",
   "metadata": {},
   "outputs": [],
   "source": [
    "from multi_join import join"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "74ee1858",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[['A'], ['A']]\n",
      "[['B'], ['B', 'B']]\n",
      "[['C', 'C'], ['C']]\n",
      "[['D'], ['D']]\n"
     ]
    }
   ],
   "source": [
    "a = \"ABCCD\"\n",
    "b = \"ABBCD\"\n",
    "\n",
    "it_a = join.JoinIterator(a)\n",
    "it_b = join.JoinIterator(b)\n",
    "\n",
    "for row in join.Join(it_a, it_b):\n",
    "    print(row)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "925a3640",
   "metadata": {},
   "source": [
    "The output above demonstrates how join yields it's joined rows. You will notice that each row is a `list` containing two sub lists. The position of the sub list within each row list represents the character that is joined from the respective iterator. So at position `[0]` there is the joined character from `it_a` (the first iterator that was provided) and at position `[1]` is the output of the second iterator. In this example every character in one string has a corresponding character in the other string. However, you will also notice that the output is flattened to be unique for every character (unique key). So, `it_b`, has two `B` characters that join to the single `B` character in `it_a`, these are represented in the sub-list at position `[1]` in row `[1]` (the second row). A similar situation exists for the character `C` in `in_a`. There is a helper function to expand these flattened outputs that is demonstrated below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "5e777244",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[['A', 'A']]\n",
      "[['B', 'B'], ['B', 'B']]\n",
      "[['C', 'C'], ['C', 'C']]\n",
      "[['D', 'D']]\n"
     ]
    }
   ],
   "source": [
    "a = \"ABCCD\"\n",
    "b = \"ABBCD\"\n",
    "\n",
    "it_a = join.JoinIterator(a)\n",
    "it_b = join.JoinIterator(b)\n",
    "\n",
    "for row in join.Join(it_a, it_b):\n",
    "    print(join.expand_join_block(row))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1366b5b0",
   "metadata": {},
   "source": [
    "The function `multi_join.join.expand_join_block`, expands each joined row (block) to represent the product combination of all the joins, this is similar to what you would get from a unix join command or a relational database join. It is not so evident from this example but it will be demonstrated again in later examples."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "15eeb6e7",
   "metadata": {},
   "source": [
    "### A three-way join\n",
    "Now, a third string will be added, this will only have a few characters represented in the previous strings. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "26e78462",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[['A'], ['A'], ['A']]\n",
      "[['C', 'C'], ['C'], ['C']]\n"
     ]
    }
   ],
   "source": [
    "a = \"ABCCD\"\n",
    "b = \"ABBCD\"\n",
    "c = \"AC\"\n",
    "\n",
    "it_a = join.JoinIterator(a)\n",
    "it_b = join.JoinIterator(b)\n",
    "it_c = join.JoinIterator(c)\n",
    "\n",
    "for row in join.Join(it_a, it_b, it_c):\n",
    "    print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3ee8fcb8",
   "metadata": {},
   "source": [
    "So, compared with the basic join above, we have lost some joins. Under default conditions with no join flags, in order for a join to be valid and the join to be returned, the characters (objects) must be present in all of the strings, we can see that `B` and `D` are only present in the first two strings (`a`, `b`) and not in the third one (`c`). However, by passing flags to the `join_condition` of the iterator we can change the default behaviour."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b5696f62",
   "metadata": {},
   "source": [
    "### The `multi_join.join.INTERSECT` flag\n",
    "In the examples above we implicitly used the flag `multi_join.join.NO_CONDITION` which is the default. However, we will now re-run the three-way join above but with the `multi_join.join.INTERSECT` flag applied to some of our iterators. Imagine if we are _really_ interested in if there are joins between strings `a` and `b` and but loosly interested in joins with `c` i.e. we want to see them if they exist not we really want to see joins between `a` and `b` even if they do not join with `c`. This sort of scenario is what the `INTERSECT` flag is for. We can apply it to our iterators of interest and whenever they join we will get a result, we will also see the results from `c` irrespective of an `a`-`b` join. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "75b059cf",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[['A'], ['A'], ['A']]\n",
      "[['B'], ['B', 'B'], []]\n",
      "[['C', 'C'], ['C'], ['C']]\n",
      "[['D'], ['D'], []]\n"
     ]
    }
   ],
   "source": [
    "a = \"ABCCD\"\n",
    "b = \"ABBCD\"\n",
    "c = \"AC\"\n",
    "\n",
    "it_a = join.JoinIterator(a, join_condition=join.INTERSECT)\n",
    "it_b = join.JoinIterator(b, join_condition=join.INTERSECT)\n",
    "it_c = join.JoinIterator(c)\n",
    "\n",
    "for row in join.Join(it_a, it_b, it_c):\n",
    "    print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a9925f55",
   "metadata": {},
   "source": [
    "Now we have a result that is a combination of the results from the basic join and the three-way join. We have the characters `B` and `D` included as they both occur in `a` and `b` and we applied the `INTERSECT` flag to their respective iterators."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5c8a9908",
   "metadata": {},
   "source": [
    "### The `multi_join.join.RETURN_ALL` flag\n",
    "The `RETURN_ALL` flag acts in a similar way to a left/right join in a relational database. As the name suggests it will return all the objects from any iterator it is applied to regardless of a join with other iterators. For this example we will perform a four-way join and add a longer string which we will apply our `RETURN_ALL` flag to. We will also leave the `INTERSECT` flags in place on `a` and `b`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "1fbd23fa",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[['A'], ['A'], ['A'], []]\n",
      "[['B'], ['B', 'B'], [], ['B']]\n",
      "[['C', 'C'], ['C'], ['C'], ['C', 'C', 'C']]\n",
      "[['D'], ['D'], [], []]\n",
      "[[], [], [], ['E']]\n",
      "[[], [], [], ['F']]\n",
      "[[], [], [], ['I']]\n",
      "[[], [], [], ['J']]\n",
      "[[], [], [], ['Z', 'Z']]\n"
     ]
    }
   ],
   "source": [
    "a = \"ABCCD\"\n",
    "b = \"ABBCD\"\n",
    "c = \"AC\"\n",
    "d = \"BCCCEFIJZZ\"\n",
    "\n",
    "it_a = join.JoinIterator(a, join_condition=join.INTERSECT)\n",
    "it_b = join.JoinIterator(b, join_condition=join.INTERSECT)\n",
    "it_c = join.JoinIterator(c)\n",
    "it_d = join.JoinIterator(d, join_condition=join.RETURN_ALL)\n",
    "\n",
    "for row in join.Join(it_a, it_b, it_c, it_d):\n",
    "    print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ddc2dae3",
   "metadata": {},
   "source": [
    "Here we can see that all the intersecting rows between `a` and `b` are still present alongside any incidental joins they have with `c` and `d` but we can also see that all the characters from `d` are present irrespective of joins with `a`, `b` and `c`. With this output it will be easier to see the impact of expanding the rows, so lets do that and view it with pandas. Note, that if you were joining elements from a list you would have to join all the lists up as well (or extract specific data from them)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "6ba7db9c",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "3e2a9e9b",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>a</th>\n",
       "      <th>b</th>\n",
       "      <th>c</th>\n",
       "      <th>d</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>A</td>\n",
       "      <td>A</td>\n",
       "      <td>A</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>B</td>\n",
       "      <td>B</td>\n",
       "      <td>None</td>\n",
       "      <td>B</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>B</td>\n",
       "      <td>B</td>\n",
       "      <td>None</td>\n",
       "      <td>B</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>C</td>\n",
       "      <td>C</td>\n",
       "      <td>C</td>\n",
       "      <td>C</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>C</td>\n",
       "      <td>C</td>\n",
       "      <td>C</td>\n",
       "      <td>C</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>5</th>\n",
       "      <td>C</td>\n",
       "      <td>C</td>\n",
       "      <td>C</td>\n",
       "      <td>C</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>6</th>\n",
       "      <td>C</td>\n",
       "      <td>C</td>\n",
       "      <td>C</td>\n",
       "      <td>C</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>7</th>\n",
       "      <td>C</td>\n",
       "      <td>C</td>\n",
       "      <td>C</td>\n",
       "      <td>C</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>8</th>\n",
       "      <td>C</td>\n",
       "      <td>C</td>\n",
       "      <td>C</td>\n",
       "      <td>C</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>9</th>\n",
       "      <td>D</td>\n",
       "      <td>D</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10</th>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>E</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>11</th>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>F</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>12</th>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>I</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>13</th>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>J</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>14</th>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>Z</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>15</th>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>Z</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "       a     b     c     d\n",
       "0      A     A     A  None\n",
       "1      B     B  None     B\n",
       "2      B     B  None     B\n",
       "3      C     C     C     C\n",
       "4      C     C     C     C\n",
       "5      C     C     C     C\n",
       "6      C     C     C     C\n",
       "7      C     C     C     C\n",
       "8      C     C     C     C\n",
       "9      D     D  None  None\n",
       "10  None  None  None     E\n",
       "11  None  None  None     F\n",
       "12  None  None  None     I\n",
       "13  None  None  None     J\n",
       "14  None  None  None     Z\n",
       "15  None  None  None     Z"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "a = \"ABCCD\"\n",
    "b = \"ABBCD\"\n",
    "c = \"AC\"\n",
    "d = \"BCCCEFIJZZ\"\n",
    "\n",
    "it_a = join.JoinIterator(a, join_condition=join.INTERSECT)\n",
    "it_b = join.JoinIterator(b, join_condition=join.INTERSECT)\n",
    "it_c = join.JoinIterator(c)\n",
    "it_d = join.JoinIterator(d, join_condition=join.RETURN_ALL)\n",
    "\n",
    "all_joins = []\n",
    "for row in join.Join(it_a, it_b, it_c, it_d):\n",
    "    all_joins.extend(join.expand_join_block(row))\n",
    "pd.DataFrame(all_joins, columns=[\"a\", \"b\", \"c\", \"d\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "df788fe5",
   "metadata": {},
   "source": [
    "### Iterator monitors\n",
    "We have covered the impact of different join conditions, which determine what constitutes a join. However, we have not covered iteration monitoring. When you have > 2 iterators sometimes you might not want the join to run all the way to the end of the last iterator. You may be joining one dataset to a master data set to incorporate aditioal fields. This is where iterator monitors can be used. To illustrate this we will start with a new set of strings and use the `multi_join.join.AnyIterMonitor`, this can be applied to one or more iterators and when any of them are exhausted, then a signal is given for the join process to terminate, this is useful in conjuction with the `RETURN_ALL` join condition but can be used without it. There is also a `multi_join.join.AllIterMonitor` that works in a simialr way but requires all of the iterators to be exhausted before the signal to terminate the join process is given."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "583542f9",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>a</th>\n",
       "      <th>b</th>\n",
       "      <th>c</th>\n",
       "      <th>d</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>C</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>C</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>D</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>D</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>E</td>\n",
       "      <td>E</td>\n",
       "      <td>None</td>\n",
       "      <td>E</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>H</td>\n",
       "      <td>None</td>\n",
       "      <td>H</td>\n",
       "      <td>H</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "   a     b     c  d\n",
       "0  C  None  None  C\n",
       "1  D  None  None  D\n",
       "2  E     E  None  E\n",
       "3  H  None     H  H"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "a = \"CDEH\"\n",
    "b = \"ABE\"\n",
    "c = \"FGHIJKLMO\"\n",
    "d = \"ABCDEFGHIJKLMNOPQRSTUVWXYZ\"\n",
    "\n",
    "monitor = join.AnyIterMonitor()\n",
    "\n",
    "it_a = join.JoinIterator(a, monitor=monitor, join_condition=join.RETURN_ALL)\n",
    "it_b = join.JoinIterator(b)\n",
    "it_c = join.JoinIterator(c)\n",
    "it_d = join.JoinIterator(d)\n",
    "\n",
    "all_joins = []\n",
    "for row in join.Join(it_a, it_b, it_c, it_d):\n",
    "    all_joins.extend(join.expand_join_block(row))\n",
    "pd.DataFrame(all_joins, columns=[\"a\", \"b\", \"c\", \"d\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "748ee0bf",
   "metadata": {},
   "source": [
    "Here we can see that all the characters in `a` are returned as the `RETURN_ALL` condition is applied, _and_ the join has stopped after the last character of `a`. If we didn't apply this we would not have had any more hits but the join process would carry on going until the end of `d`. This is because, as it is currently implemented the join process assumes that there may be other flags applied so will exhaust all iterators unless told otherwise.\n",
    "\n",
    "One other thing to remember, is that the same monitor object should be applied to all the iterators that you want to monitor, so you should not do things like `joinIterator(a, monitor=join.AnyIterMonitor())` if you want to monitor > 1 iterator. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "516379fb",
   "metadata": {},
   "source": [
    "## Summary\n",
    "The main functionallity of the `multi_join` package has been covered but only in very simple examples. This tutorial has not delved into the join `key`, but I will update in the not too distant future. Also, examples illustrating joins of lists of lists and lists of dicts will be added in future along with things like using `csv.reader` and `csv.DictReader` iterators."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
