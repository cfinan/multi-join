.. multi-join documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to multi-join
=====================

multi-join is a package that enables users to perform N-way joins of Python iterators or objects that can be made into iterators. There are options to control what constitutes a join and for terminating the join process when one or a set of iterables is exhausted.

Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Setup

   getting_started

.. toctree::
   :maxdepth: 2
   :caption: Example code

   api_usage

.. toctree::
   :maxdepth: 2
   :caption: Programmer reference

   join

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
