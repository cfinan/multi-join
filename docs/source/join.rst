``multi_join`` API
==================

``multi_join.join`` module
--------------------------

.. automodule:: multi_join.join
   :members:
   :undoc-members:
   :show-inheritance:
   :member-order: bysource
