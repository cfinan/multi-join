"""Perform N-way join over multiple files
"""
from operator import methodcaller
from collections.abc import Iterator
import heapq
import inspect
import pprint as pp


NO_CONDITION = 0
"""No join condition (`int`).

Notes
-----
``JoinIterator`` objects with this will only return rows if if the join exists
 across all files or if another file has a row that will be returned anyway
(i.e. ``multi_join.join.RETURN_ALL`` or a subset of files with a specific
``multi_join.join.RETURN_ALL`` condition).
"""
RETURN_ALL = 1
"""Return all condition (`int`).

Notes
-----
``JoinIterator`` objects with this will return their rows irrespective of their
joining with another file. This is similar to a left or right join.
"""
INTERSECT = 2
"""Intersect condition (`int`).

Notes
-----
``JoinIterator`` objects with this will only return their rows if they join
with other files that also have the intersect condition or they join with files
that have rows that would have been returned anyway.
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class AllIterMonitor(object):
    """The job of the ALL join monitor is to receive signals from Iterators
    that have reached the end of their sequence and when ALL of the "watched"
    Iterators have finished their iteration this terminates the join run. This
    monitor will only terminate if ALL of the Iterators on the watch list have
    finished iterating.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self):
        # Will hold all the files that need watching
        self._watch_iterators = []
        self._join = None

        # Log a count of the number of watched files that have finished
        # iterating through their content
        self._finished_iterators = 0

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __len__(self):
        return len(self._watch_iterators)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_watch(self, iterator_to_watch):
        """Register an iterator on the watch list.

        Parameters
        ----------
        iterator_to_watch : `multi_join.join.JoinIterator`
            The join iterator that will be added to the watch list.
        """
        # print(f"Watching: {iterator_to_watch}: {len(self)}")
        # print(self._watch_iterators)
        self._watch_iterators.append(iterator_to_watch)
        # print(f"Watching #{len(self)}")
        # print(self._watch_iterators)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_watch(self):
        """Register an iterator on the watch list.

        Returns
        -------
        watched_iterators : `list` of `multi_join.join.JoinIterator`
            The iterators that are being watched.
        """
        return self._watch_iterators

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_join(self, join_obj):
        """Register a join object with the monitor. When the monitor indicates
        that the join has finished it will call terminate on the join object.

        Parameters
        ----------
        join_obj : `multi_join.join.Join`
            A join object to register.

        Raises
        ------
        KeyError
            If an attempt is made to re-set the join object.
        TypeError
            If ``join_obj`` is not a subclass of ``multi_join.join.Join``.

        Notes
        -----
        This can only be set once and can't be reset.
        """
        if self._join is not None:
            raise KeyError("join object can only be added once")

        if not issubclass(join_obj.__class__, Join):
            raise TypeError("Can only set instances of Join")

        self._join = join_obj

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def iterator_finished(self, finished_iterator):
        """Register an iterator as finished.

        Parameters
        ----------
        finished_iterator : `multi_join.join.JoinIterator`
            The join iterator that has finished iterating. If the iterator is
            on the watch list then the fact that it has finsihed iterating is
            logged.

        Raises
        ------
        StopIteration
            If no join object has been set and all the registered iterators in
            the watch list have finished iterating.

        Notes
        -----
        If a join object has been set then this will not raise an error but
        will call terminate on the join object.
        """
        if finished_iterator in self.get_watch():
            self._finished_iterators += 1
            # print(f"# WATCHED ITERATORS: {len(self._watch_iterators)}")
            # print(f"# FINISHED ITERATORS: {self._finished_iterators}")
            if self._finished_iterators == len(self._watch_iterators):
                try:
                    # print("TERMINATE CALLED!!")
                    self._join.terminate()
                except AttributeError:
                    raise StopIteration("monitor terminating: all")


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class AnyIterMonitor(AllIterMonitor):
    """The job of the ANY join monitor is to receive signals from Iterators
    that have reached the end of their sequence and when ONE of the "watched"
    Iterators has finished their iteration this terminates the join run. This
    monitor will only terminate if ONE of the Iterators on the watch list have
    finished iterating.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def iterator_finished(self, finished_iterator):
        """Register an iterator as finished.

        Parameters
        ----------
        finished_iterator : `multi_join.join.JoinIterator`
            The join iterator that has finished iterating. If the iterator is
            on the watch list then the fact that it has finished iterating is
            logged.

        Raises
        ------
        StopIteration
            If no join object has been set and all the registered iterators in
            the watch list have finished iterating.

        Notes
        -----
        If a join object has been set then this will not raise an error but
        will call terminate on the join object.
        """
        if finished_iterator in self._watch_iterators:
            try:
                self._join.terminate()
            except AttributeError:
                raise StopIteration("monitor terminating: any")


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class JoinIterator(object):
    """A wrapper around an iterator. This handles iterating through rows from
    the file and evaluating join condition for the iterator.

    Parameters
    ----------
    iterator : `iterator`
        The pre-sorted iterator to be used in the join. If the iterator is not
        actually an iterator, i.e a ``list``, an attempt is made to cast it
        into an iterator. If this fails a TypeError is raised. However, beware,
        objects such as sets and possibly dicts may not return their contents
        in the expected sequence. IMPORTANT: The iterator must be pre-sorted on
        your keys before being joined, similar to a unix join.
    key : `class` or `function` or `tuple`
        A method of getting the key from the row. If it is a class it should
        accept a row (a delimited split ``list``) and the parent file object
        and expose a `get_key()` method that should provide a tuple to join on.
        If a function then it should provide a tuple of values to join on.
        If key is a `tuple` of `int` then it is assumed that these are column
        numbers (`list` indexes) that need returning but if they are strings
        it is assumed that they are column names that should be in the header.
    monitor : `join.AllIterMonitor` or `join.AnyIterMonitor` or `NoneType`
        A monitor class that will define when Join exits. ``JoinIterators``
        with `join.AllIterMonitor` will requite that all the Iterators in the
        monitor are finished before raising an exit condition. Those with
        ``join.AnyIterMonitor``, require that only one of the Iterators in the
        monitor exits to raise an exit condition. Those that are ``NoneType``
        are passive and will iterate until an exit condition is reached or
    join_condition : `int`, optional, default: `join.NO_CONDITION`
        A constant indicating how rows from this file will be joined and or
        returned. NO_CONDITION will mean that rows from this file will only be
        returned if they occur across all the files, or they join with a file
        that has a join condition that will be returned anyway
        (i.e. RETURN_ALL). RETURN_ALL means that rows from this file will
        will be returned irrespective of having a join with another file.
        This is similar to a left or right join and INTERSECT means that rows
        from this file will only be returned if they join with other files with
        an INTERSECT condition.

    TypeError
        If the ``iterator`` is not an iterator and can't be made into one or if
        the monitor is not a subclass of AllIterMonitor.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, iterator, key=None, monitor=None,
                 join_condition=NO_CONDITION):
        self.iterator = iterator
        self.key = key
        self.monitor = monitor
        self._infile_obj = None
        self._idx = None
        self.join_condition = join_condition

        # Check if the input is truly an iterator
        if not isinstance(self.iterator, Iterator):
            try:
                self.iterator = iter(self.iterator)
            except TypeError as e:
                raise TypeError(
                    "not an iterator and can't be made into one"
                ) from e

        if self.join_condition not in [NO_CONDITION, RETURN_ALL, INTERSECT]:
            raise ValueError(f"unknown condition type: {self.join_condition}")

        if monitor is not None:
            if issubclass(monitor.__class__, AllIterMonitor) is False:
                raise TypeError("monitors must inherit from AllIterMonitor")

            self.monitor.set_watch(self)

        # Now setup the key class
        self._set_key_class()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def idx(self):
        """Return the file number in the join (`int`)

        See also
        --------
        set_idx
        """
        return self._idx

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_idx(self, idx):
        """A one time setter for the file number in the join sequence. This will
        raise an error if called twice.

        Parameters
        ----------
        idx : `int`
            The index that the file will be set to

        Raises
        ------
        AttributeError
            If set_idx has already been called
        """
        if self._idx is not None:
            raise AttributeError("can only set once")
        self._idx = idx

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __iter__(self):
        """Initialise the iterator
        """
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __next__(self):
        """Return the next row

        Returns
        -------
        key_class : `JoinRow`
            An object wrapping the row in the file. This object exposes a
            ``get_key`` method used by Join to determine the data to be joined
            on.
        """
        try:
            # Grab the line and wrap it in the join row class, the line is
            # supplied as a list
            return self.key_class(next(self.iterator), self)
        except StopIteration as e:
            # Reached the end of the iterator so if this file is being
            # monitored for the join exit condition then we register it
            if self.monitor is not None:
                # print(f"Iterator {self.idx} finshed!!")
                self.monitor.iterator_finished(self)
            raise StopIteration("iterator stop: {0}", self.idx) from e

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _set_key_class(self):
        """Setup the key class that will wrap the rows and provide the keys for
        the join.

        If the user has supplied a class it is used as is. If it is a function
        then it is wrapped into a class under the `get_key` method. If it is a
        tuple
        """
        if inspect.isclass(self.key):
            self.key_class = self.key
        elif inspect.ismethod(self.key) or inspect.isfunction(self.key):
            # User supplied a method or function so we create a class on the
            # fly
            self.key_class = type(
                "_JoinRowFunc", (), dict(
                    __init__=_remote_init,
                    __repr__=_remote_repr,
                    get_key=_remote_func(self.key)
                )
            )
        elif isinstance(self.key, tuple):
            # If the user has supplied a tuple as the key. It could be a tuple
            # of `ints` (column numbers), strings (column names) or tuples
            # (where [0] is a column name or number and [1] is a data type or
            # transformation function)

            # Here we make sure all the keys have a data type associated with
            # them with str being the default data type.
            self.key = [
                i if isinstance(i, tuple) else (i, None) for i in self.key
            ]

            # Are the keys column names?
            if not all([isinstance(i[0], str) for i in self.key]) and \
               not all([isinstance(i[0], int) for i in self.key]):
                raise TypeError("can't have column keys of mixed type")

            # Now build the dynamic class and get_key function which will be
            # tagged to the get_key method
            self.key_class = type(
                "_JoinRowExec", (), dict(
                    __init__=_remote_init,
                    __repr__=_remote_repr
                )
            )
            _create_func(self.key_class, self.key)
        elif self.key is None:
            # User supplied a method or function so we create a class on the
            # fly
            self.key_class = type(
                "_JoinRowNone", (), dict(
                    __init__=_remote_init,
                    __repr__=_remote_repr,
                    get_key=_remote_none
                )
            )
        else:
            raise TypeError(
                "can't handle keys of type({0})".format(type(self.key))
            )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Join(object):
    """Perform an N way join over pre-sorted iterators that provide rows in a
    sorted stream.

    Parameters
    ----------
    *files
        Two of more `JoinIterator` objects to join.

    Raises
    ------
    ValueError
        If the length of files is < 2.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, *files):
        self.files = files

        # Assign the index for all the files (the file number that has been
        # passed)
        for idx, f in enumerate(self.files):
            f.set_idx(idx)

        # Make sure we have the minimal number of files
        self.nfiles = len(self.files)
        if self.nfiles < 2:
            raise ValueError("need at least two files to join")

        # TODO: Adjust this to make sure everything inherits from
        # `join.JoinFile` rather than enforcing the same type
        if all([issubclass(i.__class__, JoinIterator) for i in files]) is False:
            raise TypeError("All files must be the same type")

        # Required joins (probably should be dependent joins) are sets of
        # files that have to all have a join row inorder to trigger a return
        # of a row (unless something else is being returned anyway)
        self._required_joins = \
            [i.join_condition == INTERSECT for i in self.files]
        self._nrequired_joins = sum(self._required_joins)

        # The return mask is a boolean list that indicates if any of the
        # files in the current set of join rows is destined to be returned. A
        # master copy is created here and copied for each return decision. The
        # positions of all RETURN_ALL files are preset to True in the master
        # copy
        self._return_mask = \
            [i.join_condition == RETURN_ALL for i in self.files]

        if self._nrequired_joins == 1:
            raise ValueError("INTERSECTION needs > 1 file")
        elif self._nrequired_joins == 0:
            self._nrequired_joins = len(self.files)

        # Gather the monitors from the files and add the this join object to
        # them
        for m in set([i.monitor for i in files if i.monitor is not None]):
            m.set_join(self)

        self._row_buffer = None
        # Setup the heapq merge
        self._merge = heapq.merge(*self.files, key=methodcaller('get_key'))
        self._loop = True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def terminate(self):
        """Close all the files
        """
        self._loop = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __iter__(self):
        """Initialise the iteration
        """
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __next__(self):
        """Return the next join block

        Returns
        -------
        join_block : list of list
            The length of the outer list is the same as the number of files in
            the join. The position of the sublist matches the order that the
            files were given. If a file has not provided any rows to the join
            then the sublist will have a length of zero.
        """
        while self._loop:
            # The list that will contain all the joined rows, note a nested
            # list will be added for each JoinFile within the join, rows will
            # be added to these as necessary
            join_data = [[] for i in range(self.nfiles)]

            # Must return will monitor of any of the rows in join_data are
            # flagged as must return. If so, then the join_data will be
            # returned regardless of any joins with other rows
            must_return = [False for i in range(self.nfiles)]

            # Will be set to True for rows from files flagged as INTERSECT
            intersect = [False for i in range(self.nfiles)]

            # has joined will track the files that have contributed towards
            # the join. If all of these overlap with the intersect files then
            # the join data is returned.
            has_joined = [False for i in range(self.nfiles)]

            # Get a buffered row or a row from the heapq merge. The
            # buffered row will be used after the first iteration as a row
            # needs to be read in before the decision of what to return
            row = self._row_buffer or next(self._merge)

            # The index of the file, the file index is 0 based and the files
            # are numbered in the order they are provided to join
            # "parent" here is the JoinIterator
            idx = row.parent.idx

            # Initialise all the data holders/trackers with the first row of
            # data
            join_data[idx].append(row.row)
            has_joined[idx] = True
            intersect[idx] = self._required_joins[idx]
            must_return[idx] = self._return_mask[idx]
            # print(self._return_mask)
            # The join key, we will compare subsequent row keys to this. If
            # they match hey will be part of the join block, if not then we
            # make a decision on what to return based on the files in the
            # join block
            ref_key = row.get_key()
            # print(f"KEY: {ref_key}")
            # print(f"KEY IDX: {row.parent.idx}")
            # Loop through more rows to determine if they are in the join block
            while True:
                try:
                    row = next(self._merge)
                except StopIteration:
                    row = None
                    break
                # must_return[idx] = self._return_mask[idx]
                # print(f"   * Comparing ({row.parent.idx}): {ref_key} == {row.get_key()}: {ref_key == row.get_key()}")
                if ref_key != row.get_key():
                    # In this case it does not
                    break

                # What file did the row come from, this is so we can update the
                # correct tracker positions and make sure the row is place in
                # the correct data slot of the join_data
                idx = row.parent._idx
                join_data[idx].append(row.row)
                has_joined[idx] = True
                intersect[idx] = self._required_joins[idx]
                must_return[idx] = self._return_mask[idx]

            # If we get here, then the row does not match what we have seen
            # previously, so store the row so it is available for the next call
            self._row_buffer = row
            # print(f"ANY_RETURN: {any(must_return)}")
            # Are the files that we need to intersect intersecting? If so then
            # we return all the join_data
            if sum(intersect) >= self._nrequired_joins or \
               sum(has_joined) == self.nfiles:
                # print("CALLED INTERSECT")
                return join_data
            elif any(must_return) is True:
                # print("CALLED MUST RETURN")
                # Otherwise, are there any rows for any RETURN_ALL files
                # represented
                return join_data
            # At this point no return is satisfied so we will loop again

        # This will only get called when self._loop is False, that happens when
        # terminate is called. terminate will be called by the JoinIterators
        raise StopIteration("outside loop")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _create_func(cls, idx):
    """Create a function that will return the tuple columns based on the tuple
    definition.

    Normally, I would not do this but it allows the pre-specification of a
    function that could be run millions of times and I could not think of a
    better way to create static code on the fly that will supply the sort
    values without using loops.

    I thought about using a closure but I would have to run a for loop over
    the key spec. Here I have set the function to have a linear set of calls

    Parameters
    ----------
    cls : `class`
        A class definition that the dynamically created function will be
        placed in, as a method called ``get_key``.
    idx : `tuple` of `tuple`
        The nested tuples should have a column number (`list` index) at ``[0]``
        and a required data type at ``[1]``. If the required data type is
        ``NoneType`` then no cast is attempted on it. The required data type
        must be `int`, `float`, `str`.
    """
    holder = {}

    # Will hold the row extractions that will be returned by the function.
    return_cols = []

    # col_idx is either a list index or a string that can be used in a dict
    # col_dt is the data type that we want to cast the return value to
    for col_idx, col_dt in idx:
        if col_dt not in [int, float, str, None]:
            raise TypeError("unknown data type: '{0}'".format(col_dt))

        # If a column name has been given rather than a number, then
        # make sure it is quoted in the function
        quote = ''
        if isinstance(col_idx, str):
            quote = '"'

        if col_dt in [int, float, str]:
            # Set up the code for casting
            return_cols.append(
                '{1}(self.row[{2}{0}{2}])'.format(
                    col_idx, col_dt.__name__, quote
                )
            )
        else:
            # Set up the code for no casting
            return_cols.append(
                'self.row[{1}{0}{1}]'.format(
                    col_idx, quote
                )
            )

    # The return statement
    body = "return {0}".format(
        ",".join(return_cols)
    )
    func_def = "def get_key(self): {0}".format(body)

    exec(func_def, None, holder)
    for name, value in holder.items():
        setattr(cls, name, value)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _remote_init(self, row, parent_file):
    """A function that will act as an ``__init__`` in a dynamically created
    class.

    Note that the ``row`` and ``parent_file`` arguments will be stored inside
    the ``self`` argument.

    Parameters
    ----------
    self : `_JoinRowExec`
        The object. This is explicitly defined as a parameter here as this
        function will be buried in a dynamically created class but obviously
        stands on it's own in terms of the module.
    row : `list`
        The row from the file that the dynamic created class is storing.
    parent_file : `join.JoinFile` or `join.JoinIterator`
        The JoinFile the row was extracted from.
    """
    self.row = row
    self.parent = parent_file


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _remote_repr(self):
    """A function to act as a __repr__ dunder in a dynamicly created class.

    Parameters
    ----------
    self : `_JoinRowExec`
        The object. This is explicitly defined as a parameter here as this
        function will be buried in a dynamically created class but obviously
        stands on it's own in terms of the module.

    Returns
    -------
    formatted_row : `str`
        The ``row`` `list` formatted with `pp.pformat``
    """
    return pp.pformat(self.row)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _remote_none(self):
    """Return a function that is used for instances where the user has not
    supplied any key.

    This will return the entire row object whe the ``get_key`` method is used.

    Parameters
    ----------
    self : `_JoinRowNone`
        The join row object that will be used to return the entire row object.
    """
    return self.row


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _remote_func(x):
    """Return a function that will encapsulate the function that a user has
    passed to join.

    Parameters
    ----------
    x : `function`
        The function to encapsulate.

    Returns
    -------
    method : `function`
        A function that can be assigned to the ``get_key`` method of the join.
    """
    def _nested(self):
        return x(self.row)
    return _nested


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def expand_join_block(join_data):
    """Take a "joined" row which consists of nested lists containing join rows
    and make it into a single list of join rows.

    Parameters
    ----------
    join_data : `list` of `list`
        The nested list a position [0] will contain the join data from file 0
        etc...The join rows wrap the actual lists.

    Returns
    -------
    expanded_join_data : `list` of `list`
        The join data represented in a linear `list` in the order in which
        they appear in the nested list.
    """
    return _expand_join_block(join_data, 0)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _expand_join_block(join_data, idx):
    """Take a "joined" row which consists of nested lists containing join rows
    and make it into a single list of join rows.

    Parameters
    ----------
    join_data : `list` of `list`
        The nested list where position [0] will contain the join data from
        iterator 0 (the first iterator passed to join) etc...

    Returns
    -------
    expanded_join_data : `list` of `list`
        The join data represented in a linear `list` in the order in which
        they appear in the nested list.
    """
    expanded = []
    if len(join_data[idx]) == 0:
        join_data[idx] = [None]

    for i in join_data[idx]:
        try:
            for j in _expand_join_block(join_data, 1+idx):
                x = [i]
                x.extend(j)
                expanded.append(x)
        except IndexError:
            return [[k] for k in join_data[idx]]
    return expanded


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def flatten_join_block(join_data):
    """Take a "joined" row which consists of nested lists containing join rows
    and make it into a single list of join rows.

    Parameters
    ----------
    join_data : `list` of `list` of `list`
        The nested list a position [0] will contain the join data from file 0
        etc...The join rows wrap the actual lists.

    Returns
    -------
    flat_join_data : `list` of `list`
        The join data represented in a linear `list` in the order in which
        they appear in the nested list.
    """
    return [(idx, join_row) for idx, file_rows in enumerate(join_data) for join_row in file_rows]
