"""Tests for the join module.
"""
from multi_join import join
import pytest
import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "class_name",
    [join.AnyIterMonitor, join.AllIterMonitor]
)
def test_param(class_name):
    """Test to see that the monitors can be created.
    """
    class_name()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "iterator",
    [[1, 2, 3, 4, 5], "hello", set(["A", "B", "C"]), ('DD', 'EE')]
)
def test_iterator_cast(iterator):
    """This tests that common data types are cast to iterators without error.
    raised.
    """
    join.JoinIterator(iterator, key=None)
    # with pytest.raises(TypeError) as the_error:
    #     raise TypeError("I was expecting this error")

    # # Now make sure that the correct error massage was raised
    # assert the_error.value.args[0] == "I was expecting this error", \
    #     "wrong error massage"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "iterator",
    [[1, 2, 3, 4, 5], "hello", ('DD', 'EE')]
)
def test_iterator_iter(iterator):
    """Test that we can iterate through the iterator and a NoneType key returns
    the whole object.
    """
    for idx, i in enumerate(join.JoinIterator(iterator, key=None)):
        assert i.get_key() == iterator[idx]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "iterator",
    [
        ([dict(A=1, B=2, C=3),
          dict(A="A", B="B", C="C"),
          dict(A="hello", B="big", C="world")]),
    ]
)
def test_iterator_str_keys(iterator):
    """Test that we can iterate through the iterator and a NoneType key returns
    the whole object.
    """
    for idx, i in enumerate(join.JoinIterator(iterator, key=('A', 'C'))):
        assert i.get_key() == (iterator[idx]['A'], iterator[idx]['C'])


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "iterator",
    [
        ([['A', 'B', 'C'], ['D', 'E', 'F'], ['G', 'H', 'I'], ['J', 'K', 'L']])
    ]
)
def test_iterator_int_keys(iterator):
    """Test that we can iterate through the iterator and a NoneType key returns
    the whole object.
    """
    for idx, i in enumerate(join.JoinIterator(iterator, key=(0, 2))):
        assert i.get_key() == (iterator[idx][0], iterator[idx][2])


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "iterator",
    [
        ([dict(A=1, B=2, C=3),
          dict(A="A", B="B", C="C"),
          dict(A="hello", B="big", C="world")]),
    ]
)
def test_iterator_lambda_keys(iterator):
    """Test that we can iterate through the iterator and a NoneType key returns
    the whole object.
    """
    for idx, i in enumerate(join.JoinIterator(iterator,
                                              key=lambda x: (x['A'], x['C']))):
        assert i.get_key() == (iterator[idx]['A'], iterator[idx]['C'])


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "iterator",
    [
        ([['0.12', '1.0', '0.5'], ['2.0', '2.1', '2.6'],
          ['9.01', '5.0', '110.0'], ['44.3', '-0.1', '-1001.5']])
    ]
)
def test_iterator_float_cast(iterator):
    """Test that we can iterate through the iterator and a NoneType key returns
    the whole object.
    """
    for idx, i in enumerate(
            join.JoinIterator(iterator, key=((0, float), (2, float)))
    ):
        assert i.get_key() == (float(iterator[idx][0]), float(iterator[idx][2]))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "iterator,monitor,key,condition,exp_result",
    [
        (
            ['ABCDEFGHZZ', 'AACDDFGHZ', 'ABCDGHZ', 'ADGIKNOSTVZ'],
            [None, None, None, None],
            [None, None, None, None],
            [join.NO_CONDITION, join.NO_CONDITION,
             join.NO_CONDITION, join.NO_CONDITION],
            [
                [['A'], ['A', 'A'], ['A'], ['A']],
                [['D'], ['D', 'D'], ['D'], ['D']],
                [['G'], ['G'], ['G'], ['G']],
                [['Z', 'Z'], ['Z'], ['Z'], ['Z']],
            ]
        ),
        (
            ['ABCDEFGHZZ', 'AACDDFGHZ', 'ABCDGH', 'ADGIKNOSTVZ'],
            [None, None, join.AnyIterMonitor, None],
            [None, None, None, None],
            [join.NO_CONDITION, join.NO_CONDITION,
             join.NO_CONDITION, join.NO_CONDITION],
            [
                [['A'], ['A', 'A'], ['A'], ['A']],
                [['D'], ['D', 'D'], ['D'], ['D']],
                [['G'], ['G'], ['G'], ['G']],
            ]
        ),
        (
            ['ABCDEFGHZZ', 'AAC', 'ABCDGH', 'ADGIKNOSTVZ'],
            [None, join.AnyIterMonitor, join.AnyIterMonitor, None],
            [None, None, None, None],
            [join.NO_CONDITION, join.NO_CONDITION,
             join.NO_CONDITION, join.NO_CONDITION],
            [
                [['A'], ['A', 'A'], ['A'], ['A']],
            ]
        ),
        (
            ['ABCDEFGHZZ', 'AAC', 'ABCDGH', 'ADGIKNOSTVZ'],
            [None, join.AllIterMonitor, join.AllIterMonitor, None],
            [None, None, None, None],
            [join.NO_CONDITION, join.NO_CONDITION,
             join.NO_CONDITION, join.NO_CONDITION],
            [
                [['A'], ['A', 'A'], ['A'], ['A']],
            ]
        ),
        (
            ['ABCDEFGHZZ', 'AAC', 'ABCDGH', 'ADGIKNOSTVZ'],
            [None, join.AllIterMonitor, join.AllIterMonitor, None],
            [None, None, None, None],
            [join.INTERSECT, join.INTERSECT,
             join.NO_CONDITION, join.NO_CONDITION],
            [
                [['A'], ['A', 'A'], ['A'], ['A']],
                [['C'], ['C'], ['C'], []],
            ]
        ),
        (
            ['ABCDEFGHZZ', 'AAC', 'ABCDGH', 'ADGIKNOSTVZ'],
            [None, join.AllIterMonitor, join.AllIterMonitor, None],
            [None, None, None, None],
            [join.INTERSECT, join.INTERSECT,
             join.NO_CONDITION, join.RETURN_ALL],
            [
                [['A'], ['A', 'A'], ['A'], ['A']],
                [['C'], ['C'], ['C'], []],
                [['D'], [], ['D'], ['D']],
                [['G'], [], ['G'], ['G']],
            ]
        ),
        (
            ['ABCDEFGHZZ', 'AAC', 'ABCDGH', 'ADGIKNOSTVZ'],
            [join.AnyIterMonitor, None, None, None],
            # [None, None, None, None],
            [None, None, None, None],
            [join.INTERSECT, join.NO_CONDITION,
             join.INTERSECT, join.RETURN_ALL],
            [
                [['A'], ['A', 'A'], ['A'], ['A']],
                [['B'], [], ['B'], []],
                [['C'], ['C'], ['C'], []],
                [['D'], [], ['D'], ['D']],
                [['G'], [], ['G'], ['G']],
                [['H'], [], ['H'], []],
                [[], [], [], ['I']],
                [[], [], [], ['K']],
                [[], [], [], ['N']],
                [[], [], [], ['O']],
                [[], [], [], ['S']],
                [[], [], [], ['T']],
                [[], [], [], ['V']],
                [['Z', 'Z'], [], [], ['Z']],
            ]
        ),
    ]
)
def test_join_none(iterator, monitor, key, condition, exp_result):
    """Test that the join returns expected data when no keys are used and
    different monitors/join conditions are used.
    """
    all_jm = join.AllIterMonitor()
    any_jm = join.AnyIterMonitor()

    # print("")
    its = []
    for i, m, k, c in zip(iterator, monitor, key, condition):
        try:
            if isinstance(all_jm, m):
                m = all_jm
            elif isinstance(any_jm, m):
                m = any_jm
        except TypeError:
            pass

        its.append(
            join.JoinIterator(i, key=k, monitor=m, join_condition=c)
        )

    result = []
    for i in join.Join(*its):
        result.append(i)

    assert exp_result == result


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "raw,exp_result",
    [
        (
            [['A'], ['A', 'B'], ['C'], ['D', 'D', 'D', 'D']],
            [['A', 'A', 'C', 'D'],
             ['A', 'A', 'C', 'D'],
             ['A', 'A', 'C', 'D'],
             ['A', 'A', 'C', 'D'],
             ['A', 'B', 'C', 'D'],
             ['A', 'B', 'C', 'D'],
             ['A', 'B', 'C', 'D'],
             ['A', 'B', 'C', 'D']],
        ),
        (
            [['A'], ['A', 'B'], [], ['D', 'D', 'D', 'D']],
            [['A', 'A', None, 'D'],
             ['A', 'A', None, 'D'],
             ['A', 'A', None, 'D'],
             ['A', 'A', None, 'D'],
             ['A', 'B', None, 'D'],
             ['A', 'B', None, 'D'],
             ['A', 'B', None, 'D'],
             ['A', 'B', None, 'D']],
        ),
    ]
)
def test_expand_join_block(raw, exp_result):
    """Test that the join returns expected data when no keys are used and
    different monitors/join conditions are used.
    """
    # pp.pprint(raw)
    # pp.pprint(exp_result)
    # pp.pprint(join.expand_join_block(raw))
    assert exp_result == join.expand_join_block(raw)
